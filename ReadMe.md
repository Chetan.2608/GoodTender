*How to Run Project

1. Download & Extract the .zip file.
2. Create Project into Your System with Name as "GoodTender".
3. Replace your Project "src" Folder with the Downloaded.
4. Run the Project in Angular internal Terminal with command " ng serve --open ".



*Software Requirements

1. NodeJs.
2. Angular CLI.
3. Visual Studio Code.


Note: Refer angular.io website for installation & Configuration.