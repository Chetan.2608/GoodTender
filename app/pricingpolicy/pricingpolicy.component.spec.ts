import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PricingpolicyComponent } from './pricingpolicy.component';

describe('PricingpolicyComponent', () => {
  let component: PricingpolicyComponent;
  let fixture: ComponentFixture<PricingpolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PricingpolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PricingpolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
