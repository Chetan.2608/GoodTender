import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import{ HttpClientModule} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { DropdownModule } from 'angular-bootstrap-md';
import { WavesModule } from 'angular-bootstrap-md';


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { FilterPipe} from './search/filter.pipe';
import { HighlightPipe} from './search/highlight.pipe';
import { PricingpolicyComponent } from './pricingpolicy/pricingpolicy.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    FilterPipe,
    HighlightPipe,
    PricingpolicyComponent,
    AboutusComponent,
    DashboardComponent


  ],


  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    MatButtonToggleModule,
    MatIconModule,
    AngularFontAwesomeModule,

    WavesModule.forRoot(),
    DropdownModule.forRoot(),
    MDBBootstrapModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    RouterModule.forRoot([
      {path: '', redirectTo: '/home', pathMatch:'full' },
      {path:'home',component:HomeComponent},
      {path:'search',component:SearchComponent},
      {path:'dashboard',component:DashboardComponent},
      {path:'pricingpolicy',component:PricingpolicyComponent},
      {path:'aboutus',component:AboutusComponent}
     ])




  ],


  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
