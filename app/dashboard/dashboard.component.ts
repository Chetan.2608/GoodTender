import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  inboxdata: string [];
  favdata: string [];
  impdata: string [];
  trashdata: string [];

  constructor(private httpService: HttpClient) {

    this.httpService.get('http://localhost/Angular/inboxtender.php').subscribe(
      data=>{
        this.inboxdata=data as string[];
      },
      (err: HttpErrorResponse)=>{
        console.log(err.message);
      }
    );  

    this.httpService.get('http://localhost/Angular/favtender.php').subscribe(
      data=>{
        this.favdata=data as string[];
      },
      (err: HttpErrorResponse)=>{
        console.log(err.message);
      }
    ); 

    this.httpService.get('http://localhost/Angular/imptender.php').subscribe(
      data=>{
        this.impdata=data as string[];
      },
      (err: HttpErrorResponse)=>{
        console.log(err.message);
      }
    ); 

    this.httpService.get('http://localhost/Angular/trashtender.php').subscribe(
      data=>{
        this.trashdata=data as string[];
      },
      (err: HttpErrorResponse)=>{
        console.log(err.message);
      }
    ); 

   

   }

  ngOnInit() {

    

  }

}
