import { Component } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  model: any = {};
  
  public modalRef: BsModalRef;
  constructor(private modalService: BsModalService) { 
  }
  public openModal(template: [any]) {
  this.modalRef = this.modalService.show(template);
  }

  onSubmit() {
    alert('SUCCESS!!' + JSON.stringify(this.model))
  }
  
}
