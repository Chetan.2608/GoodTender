import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  arrdata: string [];

  constructor(private httpService: HttpClient) {

  
    this.httpService.get('http://localhost/Angular/inboxtender.php').subscribe(
      data=>{
        this.arrdata=data as string[];
      },
      (err: HttpErrorResponse)=>{
        console.log(err.message);
      }
    );  

   }

  ngOnInit() {


    

  }

}
